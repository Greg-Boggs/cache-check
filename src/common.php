<?php
define('URL_MATCH', '_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,})))(?::\d{2,5})?(?:/\S*)?$_iuS');
define('AGENT', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36');
if (!function_exists('curl_init')) {
  die('Woah, you need to install CURL on this server.');
}

// Make the URL valid if http is missing, and check to make sure the result is a URL before requesting it
function wash_url($url) {

  $url = strip_tags($_GET['url']);
  $url = trim($url, '!"#$%&\'()*+,-./@:;<=>[\\]^_`{|}~');
  $url = check_plain($url);

  //check to see if the address has http add it if not.
  $url = (substr(ltrim($url), 0, 4) != 'http' ? 'http://' : '') . $url;

  if (is_url($url)) {
    return $url;
  }
}

function is_url($url) {

  return preg_match(URL_MATCH, $url);
}

function check_plain($text) {

  return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
}

function generateRandomString($length = 10) {
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString;
}

// Grab the URL
// http://www.php-mysql-tutorial.com/wikis/php-tutorial/reading-a-remote-file-using-php.aspx
function get_header($url) {
  $header = '';
  $curl_connection = curl_init();
  curl_setopt($curl_connection, CURLOPT_URL, $url);
  curl_setopt($curl_connection, CURLOPT_HEADER, TRUE);
  curl_setopt($curl_connection, CURLOPT_NOBODY, true);
  curl_setopt($curl_connection, CURLOPT_TIMEOUT, 30);
  curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, TRUE);
  curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($curl_connection, CURLOPT_USERAGENT, AGENT);
  $header = curl_exec($curl_connection);
  curl_close($curl_connection);

  return $header;
}

function header_to_array($header) {
    $header_arr = array();

    foreach (explode("\r\n", $header) as $i => $line) {
      if ($i === 0) {
        $header_arr['http_code'] = $line;
      } else {
        list ($key, $value) = array_pad(explode(': ', $line),2, null);
        if ($key) {
          $header_arr[strtolower($key)] = $value;
        }
      }
    }

  return $header_arr;
}

function check_cache($header) {
  $cached = FALSE;

  // Check for Age header which is set by Varnish and Cloudflare.
  if (isset($header['age']) && $header['age'] != 0) {
    $cached = TRUE;
  }

  // Check for Platform.sh route cache.
  if (isset($header['x-platform-cache']) && $header['x-platform-cache'] == 'HIT') {
    $cached = TRUE;
  }

  return $cached;
}
