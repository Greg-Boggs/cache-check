<?php
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/src/common.php';

use Gregwar\Captcha\CaptchaBuilder;
use Gregwar\Captcha\PhraseBuilder;

session_start();
$captcha_fail = FALSE;

if (isset($_POST['url'])) {
  if (isset($_SESSION['phrase']) && PhraseBuilder::comparePhrases($_SESSION['phrase'], $_POST['phrase'])) {
    unset($_SESSION['phrase']);
    $cached = '';

    // Check if requests without arguments are cached.
    $url = check_plain($_POST['url']);
    $header1 = get_header($url);
    $header_arr1 = header_to_array($header1);
    if (check_cache($header_arr1)) {
      $cached = 'part';
    }

    //Check if requests with Get variables are cached.
    $url = check_plain($_POST['url']) . '?' . generateRandomString() . '=' . generateRandomString();
    $header2 = get_header($url);
    $header_arr2 = header_to_array($header2);
    if (check_cache($header_arr2)) {
      $cached = 'full';
    }
  } else {
    $captcha_fail = true;
  }
}
$builder = new CaptchaBuilder;
$builder->build();
$_SESSION['phrase'] = $builder->getPhrase();
?>

<!doctype html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="stylesheet" href="assets/css/starter.css">
    <title>Website Cache Checker</title>
    <link rel="canonical" href="https://cache.gregboggs.com/" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Cache Checker" />
    <meta property="og:description" content="Check your website cache settings to see if you've cached your website correctly." />
    <meta property="og:url" content="https://cache.gregboggs.com/" />
    <meta property="og:site_name" content="Cache Checker" />
    <meta property="article:publisher" content="https://www.gregboggs.com/" />
    <meta property="article:modified_time" content="2021-01-17T19:18:27+00:00" />
    <meta property="og:image" content="🎯" />
    <meta property="og:image:width" content="1100" />
    <meta property="og:image:height" content="740" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@gregory_boggs" />
    <link rel="icon" href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>🎯</text></svg>">
  </head>
  <body>
    <div class="col-md-8 py-5 px-3 mx-auto">

      <header class="pb-3 mb-5 border-bottom">
        <h1>
          <a href="/" class="d-flex align-items-center text-dark text-decoration-none">
            <span>🎯 Website Cache Checker</span>
          </a>
        </h1>
      </header>

      <h2>Is your website cached correctly?</h2>
        <p class="lead">Caching dramatically improves how fast a website loads. Let's see how your website does.</p>
        <?php if (isset($_GET['error'])) { ?>
          <!-- Error Alert -->
          <div class="alert alert-danger alert-dismissible fade show">
            <strong>Error!</strong> A problem has been occurred while submitting your scan <?php echo check_plain($_GET['error']); ?>.
            <button type="button" class="close" data-dismiss="alert">&times;</button>
          </div>
        <?php
        }
        if ($captcha_fail == TRUE) { ?>
          <div class="alert alert-danger alert-dismissible fade show">
            <strong>Spam Check failed.</strong> Please try the Captcha again. I know it's annoying, but it slows down spam.
            <button type="button" class="close" data-dismiss="alert">&times;</button>
          </div>
        <?php
        } else if (isset($_POST['url'])) {
          if ($cached == 'full') { ?>
            <div class="alert alert-primary fade show">Congrats! Your website is fully cached.</div>
          <?php } else if ($cached == 'part') { ?>
            <div class="alert alert-warning fade show">Hrm, your website is cached, but not correctly yet. Your cache isn't ignoring query strings.</div>
          <?php } else { ?>
            <div class="alert alert-warning fade show">Hrm, your website appears not to have a page cache.</div>
          <?php }
        } ?>

        <fieldset class="border p-3">
          <legend  class="w-auto">Check your cache</legend>

          <form class="needs-validation" action="/" method="post" novalidate>
            <div class="form-group row">
              <label for="inputUrl" class="col-sm-1 col-form-label">Website:</label>
              <div class="col-sm-10">
                <input type="url" class="form-control" name="url" id="inputUrl" placeholder="Example: https://www.gregboggs.com" required>
                <div id="validationUrlFeedback" class="invalid-feedback">
                  Please provide a valid Url.
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputCaptcha" class="col-sm-1 col-form-label">Captcha:</label>
              <div class="col-sm-10">
                <input type="text" class="form-control <?php if ($captcha_fail == TRUE) {echo 'is-invalid';} ?>" name="phrase" id="inputCaptcha" placeholder="Type image to pass the Human Test..." required>
                <div id="validationCaptchaFeedback" class="invalid-feedback">
                  Please retype the image.
                </div>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-10 mt-3">
                <img src="<?php echo $builder->inline(); ?>" />
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-10 mt-3">
                <button type="submit" class="btn btn-primary">Start Scan</button>
              </div>
            </div>
          </form>
        </fieldset>
      <?php if (isset($header1)) { ?>
        <hr class="my-5">
        <fieldset class="border p-3">
          <legend  class="w-auto">Response without a query string</legend>
            <pre><?php echo check_plain($header1); ?></pre>
        </fieldset>
      <?php } ?>
      <?php if (isset($header2)) { ?>
        <hr class="my-5">
        <fieldset class="border p-3">
          <legend  class="w-auto">Response with a query string</legend>
          <pre><?php echo check_plain($header2); ?></pre>
        </fieldset>
      <?php } ?>
      <hr class="my-5">

      <div class="row">
        <div class="col-md-6">
          <h2>How does the scan work?</h2>
          <p>I load your website with Curl and check the response header for a non-zero age header. If your website has a different cache header, feel free to <a href="https://gitlab.com/Greg-Boggs/cache-check/-/issues">open an issue</a> on GitLab.</p>
          <p>Surprised your cache failed? Facebook and many others append a $_GET parameter to links that will break your cache. So, this scanner does as well.</p>
        </div>

        <div class="col-md-6">
          <h2>Guides</h2>
          <p>Read more detailed instructions on caching and how to set up caching for your website.</p>
          <ul class="icon-list">
            <li>Todo</li>
          </ul>
        </div>
      </div>

      <hr class="my-5">

      <p class="text-muted">Created by <a href="https://www.gregboggs.com">Greg Boggs</a> with ❤️ &nbsp;in PDX.</p>
    </div>

    <script src="node_modules/jquery/dist/jquery.slim.min.js"></script>
    <script type="module" src="assets/js/starter.js"></script>
  </body>
</html>
